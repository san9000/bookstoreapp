﻿using BookStore.Core.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.Logic
{
	public interface IPersonManager
	{
		List<AuthorEntity> GetAuthors();
		List<PublisherEntity> GetPublishers();
		PersonEntity CreateOrUpdatePerson(PersonEntity person);
		PersonEntity DeletePerson(PersonEntity person);
	}

	public class PersonManager : IPersonManager
	{
		private readonly IRepositoryProvider _repositoryProvider;

		public PersonManager(IRepositoryProvider repositoryProvider)
		{
			_repositoryProvider = repositoryProvider;
		}

		public PersonEntity CreateOrUpdatePerson(PersonEntity person)
		{
			var repo = _repositoryProvider.GetRepo();
			var modified = person.Id == 0 
				? repo.Insert(person)
				: repo.Update(person);

			return modified;
		}

		public PersonEntity DeletePerson(PersonEntity person)
		{
			var repo = _repositoryProvider.GetRepo();
			var deleted = repo.Delete(person);
			return deleted;
		}

		public List<AuthorEntity> GetAuthors()
		{
			var repo = _repositoryProvider.GetRepo();
			var authors = repo.Select<AuthorEntity>(w => true).ToList();
			return authors;
		}

		public List<PublisherEntity> GetPublishers()
		{
			var repo = _repositoryProvider.GetRepo();
			var publishers = repo.Select<PublisherEntity>(w => true).ToList();
			return publishers;
		}
	}
}
