﻿using BookStore.Core.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.Logic
{
	public interface IDbManager
	{
		bool CreateDB();
	}

	public class DbManager : IDbManager
	{
		private readonly IRepositoryProvider _repositoryProvider;

		public DbManager(IRepositoryProvider repositoryProvider)
		{
			_repositoryProvider = repositoryProvider;
		}

		public bool CreateDB()
		{
			var repo = _repositoryProvider.GetRepo();
			repo.Exec(DbScriptProvider.CreateDatabase);
			return true;
		}
	}
}
