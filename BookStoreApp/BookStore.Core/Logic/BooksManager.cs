﻿using BookStore.Core.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.Logic
{
	public interface IBooksManager
	{
		List<BookEntity> FindBooks(BooksFilter filter);
		BookEntity CreateOrUpdateBook(BookEntity book);
		BookEntity DeleteBook(BookEntity book);
	}

	public class BooksManager : IBooksManager
	{
		private readonly IRepositoryProvider _repositoryProvider;
		public BooksManager(IRepositoryProvider repositoryProvider)
		{
			_repositoryProvider = repositoryProvider;
		}

		public BookEntity CreateOrUpdateBook(BookEntity book)
		{
			var repo = _repositoryProvider.GetRepo();
			var modified = book.Id == 0
				? repo.Insert(book)
				: repo.Update(book);

			return modified;
		}

		public BookEntity DeleteBook(BookEntity book)
		{
			var repo = _repositoryProvider.GetRepo();
			var deleted = repo.Delete(book);
			return deleted;
		}

		public List<BookEntity> FindBooks(BooksFilter filter)
		{
			var repo = _repositoryProvider.GetRepo();
			var books = repo.Select<BookEntity>(w =>
				(filter?.Names == null || w.Name.Contains(filter?.Names))
				&& (filter?.Descrs == null || w.Description.Contains(filter?.Descrs))
				&& (filter?.PriceMin == null || w.Price >= filter?.PriceMin)
				&& (filter?.PriceMax == null || w.Price <= filter?.PriceMax)
			).ToList();
			
			return books;
		}
	}
}
