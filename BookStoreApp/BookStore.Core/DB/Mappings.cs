﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.DB
{
	public class AuthorMap : ClassMap<AuthorEntity>
	{
		public AuthorMap()
		{
			Table("authors");
			Id(x => x.Id).GeneratedBy.Increment();
			Map(x => x.Name);
		}
	}

	public class PublisherMap : ClassMap<PublisherEntity>
	{
		public PublisherMap()
		{
			Table("publishers");
			Id(x => x.Id).GeneratedBy.Increment();
			Map(x => x.Name);
		}
	}

	public class BookMap : ClassMap<BookEntity>
	{
		public BookMap()
		{
			Table("books");
			Id(x => x.Id).GeneratedBy.Increment();
			Map(x => x.Name);
			Map(x => x.Description);
			Map(x => x.Price);

			HasOne(x => x.Publisher);
			HasManyToMany(x => x.Authors)
				.Table("bookstoauthors").ParentKeyColumn("book_id")
				.Cascade.All()
				.ChildKeyColumn("author_id")
				.Inverse()
				;
		}
	}
}
