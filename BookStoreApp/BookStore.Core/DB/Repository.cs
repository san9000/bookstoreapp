﻿using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.DB
{
	public interface IRepository
	{
		object Session { get; }

		void Exec(string sql);
		T Insert<T>(T obj) where T: Entity;
		T Update<T>(T obj) where T : Entity;
		T Delete<T>(T obj) where T : Entity;
		T GetById<T>(object id) where T : Entity;
		IEnumerable<T> Select<T>(Func<T, bool> clause) where T : Entity;
	}

	public class NHibRepository : IRepository
	{
		private readonly ISession _session;
		public NHibRepository(object session)
		{
			_session = (ISession)session;
		}

		public object Session => _session;

		

		public void Exec(string sql)
		{
			sql.Split(';').ToList().ForEach(q => {
				var query = _session.CreateSQLQuery(sql);
				query.ExecuteUpdate();
			});
		}

		public T GetById<T>(object id) where T : Entity
		{
			var rslt = _session.Get<T>(id);
			return rslt;
		}

		public T Insert<T>(T obj) where T : Entity
		{
			using (var trn = _session.BeginTransaction()){ 
				var pkey = _session.Save(obj);
				obj.SetId(pkey);
				trn.Commit();
			}

			return obj;
		}

		public IEnumerable<T> Select<T>(Func<T, bool> clause) where T : Entity
		{
			var rslt = _session.Query<T>()
				.Where(clause)
				.ToList();
			
				return rslt;
		}

		public T Update<T>(T obj) where T : Entity
		{
			using (var trn = _session.BeginTransaction())
			{
				var merged = _session.Merge(obj);
				trn.Commit();

				return merged;
			}
		}

		public T Delete<T>(T obj) where T : Entity
		{
			using (var trn = _session.BeginTransaction())
			{
				_session.Delete(obj);
				trn.Commit();

				return obj;
			}
		}
	}
}
