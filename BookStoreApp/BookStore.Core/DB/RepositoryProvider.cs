﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.DB
{
	public interface IRepositoryProvider
	{
		IRepository GetRepo();
	}

	public class RepositoryProvider : IRepositoryProvider
	{
		private readonly IStorageProvider _connectionProvider;
		private static ISessionFactory _nhibSessionFactory;

		private ISessionFactory BuildNHibSessionFactory()
		{
			var rslt = Fluently.Configure()
				.Database(SQLiteConfiguration.Standard
					.UsingFile(_connectionProvider.ConnectionString))
					.Mappings(m => m.FluentMappings.AddFromAssemblyOf<BookEntity>())
				.BuildSessionFactory();

			return rslt;
		}

		private ISessionFactory NHibSessionFactory
		{
			get {
				_nhibSessionFactory = _nhibSessionFactory ?? BuildNHibSessionFactory();
				return _nhibSessionFactory;
			}
		}

		public RepositoryProvider(IStorageProvider connectionProvider)
		{
			_connectionProvider = connectionProvider;
		}

		public IRepository GetRepo()
		{
			var ses = NHibSessionFactory.OpenSession();
			var repo = new NHibRepository(ses);
			return repo;
		}
	}
}
