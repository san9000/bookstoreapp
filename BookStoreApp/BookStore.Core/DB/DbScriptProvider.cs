﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.DB
{
	public static class DbScriptProvider
	{
		public static string CreateDatabase => @"
DROP TABLE IF EXISTS bookstoauthors;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS publishers;



CREATE TABLE authors (
    id   INTEGER       PRIMARY KEY AUTOINCREMENT
                       NOT NULL,
    name VARCHAR (120) UNIQUE
);


CREATE TABLE publishers (
    id   INTEGER       PRIMARY KEY AUTOINCREMENT
                       NOT NULL,
    name VARCHAR (120) UNIQUE
);


CREATE TABLE books (
    id           INTEGER       PRIMARY KEY AUTOINCREMENT
                               NOT NULL,
    name         VARCHAR (400) UNIQUE,
    description  TEXT,
    price        NUMERIC,
    publisher_id INTEGER       REFERENCES publishers (id) 
);


CREATE TABLE bookstoauthors (
    book_id   INTEGER REFERENCES books (id),
    author_id INTEGER REFERENCES authors (id) 
);

INSERT INTO authors(name)VALUES('Лермонтов');
INSERT INTO authors(name)VALUES('Пушкин');
INSERT INTO publishers(name)VALUES('ТОО Издательский ДомЪ');
INSERT INTO books(name, description,price,publisher_id)VALUES('Сказки', '...', 1000, 1);
INSERT INTO bookstoauthors(book_id, author_id)VALUES(1,2);
		";
	}
}
