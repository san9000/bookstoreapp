﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core
{
	public class BooksFilter: Entity
	{
		public string Names { get; set; }
		public string Descrs { get; set; } 
		public int? PriceMin { get; set; } 
		public int? PriceMax { get; set; } 
		public string Authors { get; set; }
		public string Publisher { get; set; }
	}
}
