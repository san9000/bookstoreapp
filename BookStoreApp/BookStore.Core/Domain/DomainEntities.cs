﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core
{
	public abstract class Entity 
	{
		protected object _id;

		public virtual void SetId(object value)
		{
			_id = value;
		}
	}
	
	public abstract class Entity<TKey>: Entity
	{
		public virtual TKey Id {
			get { return _id == null ? default(TKey) : (TKey)_id; }
			set { SetId(value); }
		}
	}

	public class PersonEntity : Entity<long>
	{
		public virtual string Name { get; set; }
	}

	public class AuthorEntity : PersonEntity
	{
	}

	public class PublisherEntity : PersonEntity
	{
	}

	public class BookEntity : Entity<long>
	{
		public BookEntity()
		{
			Authors = new List<AuthorEntity>();
		}

		public virtual string Name { get; set; }
		public virtual string Description { get; set; }
		public virtual decimal Price { get; set; }
		public virtual IList<AuthorEntity> Authors { get; set; }
		public virtual PublisherEntity Publisher { get; set; }
	}
}
