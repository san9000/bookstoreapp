﻿using BookStore.Core;
using BookStore.Core.Logic;
using BookStoreApp.Controllers;
using BookStoreApp.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;

namespace BookStore.Tests
{
	[TestFixture]
	public class BooksControllerTests
	{
		private BooksController booksController;
		private Kendo.Mvc.UI.DataSourceRequest request = new Kendo.Mvc.UI.DataSourceRequest();

		[OneTimeSetUp]
		public void SetUpOnce()
		{
			var mockBook = new Mock<IBooksManager>();
			mockBook.Setup(x => x.CreateOrUpdateBook(It.IsAny<BookEntity>()))
				.Returns<BookEntity>(m => m);
			mockBook.Setup(x => x.DeleteBook(It.IsAny<BookEntity>()))
				.Returns<BookEntity>(m => m);
			mockBook.Setup(x => x.FindBooks(It.IsAny<BooksFilter>()))
				.Returns(() => new List<BookEntity>() { new BookEntity { } });

			var mockPers = new Mock<IPersonManager>();
			mockPers.Setup(x => x.CreateOrUpdatePerson(It.IsAny<PersonEntity>()))
				.Returns<PersonEntity>(m => m);
			mockPers.Setup(x => x.GetAuthors())
				.Returns(() => new List<AuthorEntity>() { new AuthorEntity { } });
			mockPers.Setup(x => x.GetPublishers())
				.Returns(() => new List<PublisherEntity>() { new PublisherEntity { } });
			mockPers.Setup(x => x.DeletePerson(It.IsAny<PersonEntity>()))
				.Returns<PersonEntity>(m => m);

			var mockDb = new Mock<IDbManager>();
			mockDb.Setup(x => x.CreateDB()).Returns(() => true);

			booksController = new BooksController(mockBook.Object, mockPers.Object, mockDb.Object);
		}

		[Test]
		public void CreateDB_CreatedNewDB()
		{
			var rsp = (JsonResult)booksController.CreateDB();
			
			Assert.True((bool)rsp.Data);
		}

		[Test]
		public void GetAuthors_ReturnsAllAuthors()
		{
			dynamic rsp = (JsonResult)booksController.GetAuthors(request);

			Assert.IsInstanceOf<List<AuthorModel>>(rsp.Data.Data);
		}

		[Test]
		public void CreateOrUpdateAuthor()
		{
			dynamic rspAdd = (JsonResult)booksController.CreateOrUpdateAuthor(request, new AuthorModel { Name = "Author" });
			dynamic rspUpd = (JsonResult)booksController.CreateOrUpdateAuthor(request, new AuthorModel { Id = 1, Name = "NextAuthor" });

			Assert.IsInstanceOf<AuthorModel>(rspAdd.Data.Data[0]);
			Assert.IsInstanceOf<AuthorModel>(rspUpd.Data.Data[0]);
		}

		[Test]
		public void DeleteAuthor()
		{
			dynamic rsp = (JsonResult)booksController.DeleteAuthor(request, new AuthorModel { Id = 1 });

			Assert.IsInstanceOf<AuthorModel>(rsp.Data.Data[0]);
		}




		[Test]
		public void GetPublishers_ReturnsAllPublishers()
		{
			dynamic rsp = (JsonResult)booksController.GetPublishers(request);

			Assert.IsInstanceOf<List<PublisherModel>>(rsp.Data.Data);
		}

		[Test]
		public void CreateOrUpdatePublisher()
		{
			dynamic rspAdd = (JsonResult)booksController.CreateOrUpdatePublisher(request, new PublisherModel { Name = "Publisher" });
			dynamic rspUpd = (JsonResult)booksController.CreateOrUpdatePublisher(request, new PublisherModel { Id = 1, Name = "NextPublisher" });

			Assert.IsInstanceOf<PublisherModel>(rspAdd.Data.Data[0]);
			Assert.IsInstanceOf<PublisherModel>(rspUpd.Data.Data[0]);
		}

		[Test]
		public void DeletePublisher()
		{
			dynamic rsp = (JsonResult)booksController.DeletePublisher(request, new PublisherModel { Id = 1 });

			Assert.IsInstanceOf<PublisherModel>(rsp.Data.Data[0]);
		}




		[Test]
		public void GetBooks_ReturnsAllBooks()
		{
			dynamic rsp = (JsonResult)booksController.GetBooks(request);

			Assert.IsInstanceOf<List<BookModel>>(rsp.Data.Data);
		}

		[Test]
		public void CreateOrUpdateBook()
		{
			dynamic rspAdd = (JsonResult)booksController.CreateOrUpdateBook(request, new BookModel { Name = "Book" });
			dynamic rspUpd = (JsonResult)booksController.CreateOrUpdateBook(request, new BookModel { 
					Id = 1, Name = "NextBook", 
					PublisherId = 1,
					Authors = "Author1;Author2"
				}
			);

			Assert.IsInstanceOf<BookModel>(rspAdd.Data.Data[0]);
			Assert.IsInstanceOf<BookModel>(rspUpd.Data.Data[0]);
		}

		[Test]
		public void DeleteBook()
		{
			dynamic rsp = (JsonResult)booksController.DeleteBook(request, new BookModel { Id = 1 });

			Assert.IsInstanceOf<BookModel>(rsp.Data.Data[0]);
		}
	}
}
