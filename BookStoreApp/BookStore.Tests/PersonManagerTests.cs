﻿using BookStore.Core;
using BookStore.Core.DB;
using BookStore.Core.Logic;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Tests
{
	[TestFixture]
	public class PersonManagerTests
	{
		private IPersonManager _personManager;
		private IRepositoryProvider _repositoryProvider;

		[OneTimeSetUp]
		public void SetUpOnce()
		{
			var mockRepo = new Mock<IRepository>();
			mockRepo.Setup(x => x.Insert(It.IsAny<AuthorEntity>())).Returns<AuthorEntity>(e => {
				return new AuthorEntity {
					Id = 1,
					Name = e.Name
				};
			});

			var mockProv = new Mock<IRepositoryProvider>();
			mockProv.Setup(x => x.GetRepo()).Returns(() => mockRepo.Object);
			_repositoryProvider = mockProv.Object;
		}
		
		[SetUp]
		public void SetUp()
		{
			_personManager = new PersonManager(_repositoryProvider);
		}
	}
}
