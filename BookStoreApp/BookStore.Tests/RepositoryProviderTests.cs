﻿#define FULL_MODE
#undef FULL_MODE
using BookStore.Core;
using BookStore.Core.DB;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Tests
{
	[TestFixture]
	public class RepositoryProviderTests
	{
		private readonly static string _curDir = Directory.GetCurrentDirectory();
		private string _dbPath;
		private IRepositoryProvider _repositoryProvider;
		private IStorageProvider _storageProvider;

		private readonly AuthorEntity AUTHOR_1 = new AuthorEntity { Name = "FirstAuthor" };
		private readonly PublisherEntity PUB_1 = new PublisherEntity { Name = "FirstPub" };
		private readonly BookEntity BOOK_1 = new BookEntity { Name = "Book", Price = 1000 };


		[OneTimeSetUp]
		public void SeUpOnce()
		{
			var mock = new Mock<IStorageProvider>();
			mock.Setup(x => x.ConnectionString).Returns(() => _dbPath);
			_storageProvider = mock.Object;

			_dbPath = Path.Combine(_curDir, "test.db");
#if FULL_MODE
			if (File.Exists(_dbPath)) {
				File.Delete(_dbPath);
			}
#endif
		}

		[SetUp]
		public void SetUp()
		{
			_repositoryProvider = new RepositoryProvider(_storageProvider);
		}
		
		[Test]
		public void GetRepo_ReturnsInitializedRepository()
		{
			var repo = _repositoryProvider.GetRepo();

			Assert.IsInstanceOf<NHibernate.ISession>(repo.Session);
		}

#if FULL_MODE
		[Test]
		public void Repo_WorckCorrectWithGenericEntities()
		{
			var repo = _repositoryProvider.GetRepo();
			repo.Exec(DbScriptProvider.CreateDatabase);

			var author = repo.Insert(AUTHOR_1);
			var pub = repo.Insert(PUB_1);
			var book = repo.Insert(BOOK_1);

			book.Publisher = pub;
			book.Authors.Add(author);
			repo.Update(book);

			book = repo.GetById<BookEntity>(book.Id);

			Assert.AreEqual(AUTHOR_1.Name, author.Name);
			Assert.AreEqual(PUB_1.Name, pub.Name);
			Assert.AreEqual(BOOK_1.Name, book.Name);
			Assert.AreEqual(pub, book.Publisher);
			Assert.AreEqual(1, book.Authors.Count);
		}
#endif
	}
}
