using BookStore.Core.DB;
using BookStore.Core.Logic;
using BookStoreApp.Utils;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace BookStoreApp
{
	public static class UnityConfig
	{
		public static void RegisterComponents()
		{
			var container = new UnityContainer();

			container.RegisterType<IBooksManager, BooksManager>();
			container.RegisterType<IPersonManager, PersonManager>();
			container.RegisterType<IRepositoryProvider, RepositoryProvider>();
			container.RegisterType<IStorageProvider, StorageProvider>();
			container.RegisterType<IDbManager, DbManager>();

			DependencyResolver.SetResolver(new UnityDependencyResolver(container));
		}
	}
}