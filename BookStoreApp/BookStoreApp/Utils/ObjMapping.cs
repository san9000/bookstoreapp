﻿using AutoMapper;
using BookStore.Core;
using BookStoreApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStoreApp
{
	#region profiles
	public class ToModelsProfile : Profile
	{
		public ToModelsProfile()
		{
			CreateMap<AuthorEntity, AuthorModel>();
			CreateMap<PublisherEntity, PublisherModel>();
			CreateMap<BookEntity, BookModel>()
				.ForMember(
					dst => dst.Authors,
					opt => opt.MapFrom(src =>
						string.Join(";", src.Authors.Select(c => c.Name))
					)
				);
		}
	}

	public class ToEntitiesProfile : Profile
	{
		public ToEntitiesProfile()
		{
			CreateMap<AuthorModel, AuthorEntity>();
			CreateMap<PublisherModel, PublisherEntity>();
			CreateMap<BookModel, BookEntity>()
				.ForMember(
					dest => dest.Publisher,
					opt => opt.MapFrom(src => new PublisherEntity { Id = src.PublisherId })
				)
				.ForMember(
					dest => dest.Authors,
					opt => opt.Ignore()
				)
			;
		}
	}
	#endregion

	public static class ObjMapping
	{
		private static readonly IMapper _mapper;

		static ObjMapping() {
			var config = new MapperConfiguration(cfg => {
				cfg.AddProfile<ToModelsProfile>();
				cfg.AddProfile<ToEntitiesProfile>();
			});

			_mapper = config.CreateMapper();
		}
		
		public static M ToModel<M>(this Entity src) where M: Model
		{
			return _mapper.Map<M>(src);
		}

		public static E ToEntity<E>(this Model src) where E : BookStore.Core.Entity
		{
			return _mapper.Map<E>(src);
		}
	}
}