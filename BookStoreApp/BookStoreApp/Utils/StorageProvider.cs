﻿using BookStore.Core.DB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace BookStoreApp.Utils
{
	public class StorageProvider : IStorageProvider
	{
		public string ConnectionString => Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
			ConfigurationManager
				.ConnectionStrings["sqliteBooks"]
				.ConnectionString
		);
	}
}