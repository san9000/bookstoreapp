﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStoreApp.Models
{
	public class PublisherModel : Model
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}