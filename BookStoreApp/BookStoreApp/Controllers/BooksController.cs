﻿using BookStore.Core;
using BookStore.Core.Logic;
using BookStoreApp.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Linq;
using System.Web.Mvc;

namespace BookStoreApp.Controllers
{
    public class BooksController : Controller
	{
		private readonly IBooksManager _booksManager;
		private readonly IPersonManager _personManager;
		private readonly IDbManager _dbManager;

		#region ctor
		public BooksController(IBooksManager booksManager, IPersonManager personManager, IDbManager dbManager)
		{
			_booksManager = booksManager;
			_personManager = personManager;
			_dbManager = dbManager;
		}
		#endregion

		public ActionResult CreateDB()
		{
			var success = _dbManager.CreateDB();
			return Json(success, JsonRequestBehavior.AllowGet);
		}

		#region Authors
		public ActionResult GetAuthors([DataSourceRequest]DataSourceRequest request)
		{
			var authors = _personManager.GetAuthors();

			var rslt = authors.Select(x => x.ToModel<AuthorModel>()).ToList();
			return Json(rslt.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[AcceptVerbs("Post")]
		public ActionResult CreateOrUpdateAuthor([DataSourceRequest] DataSourceRequest request, AuthorModel model)
		{
			var args = model.ToEntity<AuthorEntity>();
			var author = _personManager.CreateOrUpdatePerson(args);

			var rslt = author.ToModel<AuthorModel>();
			return Json(new[] { rslt }.ToDataSourceResult(request, ModelState));
		}

		[AcceptVerbs("Post")]
		public ActionResult DeleteAuthor([DataSourceRequest] DataSourceRequest request, AuthorModel model)
		{
			var args = model.ToEntity<AuthorEntity>();
			var author = _personManager.DeletePerson(args);

			var rslt = author.ToModel<AuthorModel>();
			return Json(new[] { rslt }.ToDataSourceResult(request, ModelState));
		}
		#endregion

		#region Publishers
		public ActionResult GetPublishers([DataSourceRequest]DataSourceRequest request)
		{
			var pubs = _personManager.GetPublishers();

			var rslt = pubs.Select(x => x.ToModel<PublisherModel>()).ToList();
			return Json(rslt.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[AcceptVerbs("Post")]
		public ActionResult CreateOrUpdatePublisher([DataSourceRequest]DataSourceRequest request, PublisherModel model)
		{
			var args = model.ToEntity<PublisherEntity>();
			var pub = _personManager.CreateOrUpdatePerson(args);

			var rslt = pub.ToModel<PublisherModel>();
			return Json(new[] { rslt }.ToDataSourceResult(request, ModelState));
		}

		[AcceptVerbs("Post")]
		public ActionResult DeletePublisher([DataSourceRequest]DataSourceRequest request, PublisherModel model)
		{
			var args = model.ToEntity<PublisherEntity>();
			var pub = _personManager.DeletePerson(args);

			var rslt = pub.ToModel<PublisherModel>();
			return Json(new[] { rslt }.ToDataSourceResult(request, ModelState));
		}
		#endregion

		#region Books
		public ActionResult GetBooks([DataSourceRequest]DataSourceRequest request)
		{
			var filters = request.Filters?.Cast<Kendo.Mvc.FilterDescriptor>() ?? new Kendo.Mvc.FilterDescriptor[] { };
			var books = _booksManager.FindBooks(new BooksFilter { 
				Authors = (string)filters.SingleOrDefault(x => x.Member == "Authors")?.Value,
				Names = (string)filters.SingleOrDefault(x => x.Member == "Names")?.Value,
				Descrs = (string)filters.SingleOrDefault(x => x.Member == "Descrs")?.Value,
				PriceMin = (int?)filters.SingleOrDefault(x => x.Member == "PriceMin")?.Value,
				PriceMax = (int?)filters.SingleOrDefault(x => x.Member == "PriceMax")?.Value,
			});

			var rslt = books.Select(x => x.ToModel<BookModel>()).ToList();
			return Json(rslt.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[AcceptVerbs("Post")]
		public ActionResult CreateOrUpdateBook([DataSourceRequest]DataSourceRequest request, BookModel model)
		{
			var args = model.ToEntity<BookEntity>();
			var book = _booksManager.CreateOrUpdateBook(args);

			var rslt = book.ToModel<BookModel>();
			return Json(new[] { rslt }.ToDataSourceResult(request, ModelState));
		}

		[AcceptVerbs("Post")]
		public ActionResult DeleteBook([DataSourceRequest]DataSourceRequest request, BookModel model)
		{
			var args = model.ToEntity<BookEntity>();
			var book = _booksManager.DeleteBook(args);

			var rslt = book.ToModel<BookModel>();
			return Json(new[] { rslt }.ToDataSourceResult(request, ModelState));
		}
		#endregion
	}
}
