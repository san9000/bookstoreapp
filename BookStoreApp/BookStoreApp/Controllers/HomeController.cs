﻿using BookStore.Core.Logic;
using BookStoreApp.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStoreApp.Controllers
{
	// https://www.telerik.com/aspnet-mvc/grid?_ga=2.200274478.1878084628.1586233140-1421284575.1586233140
	// https://demos.telerik.com/aspnet-core/grid/filter-row
	public class HomeController : Controller
	{
		private readonly IPersonManager _personManager;

		public HomeController(IPersonManager personManager)
		{
			_personManager = personManager;
		}
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		public ActionResult Authors()
		{
			ViewBag.Message = "Авторы.";

			return View();
		}

		public ActionResult Publishers()
		{
			ViewBag.Message = "Издатели.";

			return View();
		}

		public ActionResult Books()
		{
			ViewBag.Message = "Книги.";
			ViewData["publishers"] = _personManager
				.GetPublishers()
				.Select(x => new { PublisherId = x.Id, PublisherName = x.Name })
				.ToList();
			return View();
		}
	}
}